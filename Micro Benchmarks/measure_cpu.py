import sys
import time
import datetime
import psutil

# default time is 30 min
time_in_seconds = 1800

try:
	time_in_seconds = int(sys.argv[1])
except:
	print('Invalid arguments, using default values')

	
# set timestamp in filename
file_name = 'cpu_%s.csv' % int(time.time())

file = open(file_name, 'w')
header = 'time, cpu_percent\n'
file.write(header)

# loop for x seconds
starttime = time.time()
timer = 0
while timer < time_in_seconds:
	clock = datetime.datetime.now()
	cpu = psutil.cpu_percent(interval=0.9)

	record = '%s, %s\n' % (clock, cpu)
	file.write(record)
	
	# update time elapsed
	timer = time.time() - starttime

file.close()
