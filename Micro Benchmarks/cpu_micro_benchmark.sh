#!/bin/bash
times_in_seconds=1800

gcc -o primes primes.c
# Run in background
./primes $times_in_seconds &

# run benchmark for 30 minutes (in background)
python3 measure_cpu.py $times_in_seconds &