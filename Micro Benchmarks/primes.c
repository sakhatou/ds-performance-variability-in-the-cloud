#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int main(int argc, char *argv[]) {
    clock_t start_time = clock();

    // Get number of primes to find from argv
    int target_time;
    if (argc != 2) {
        printf("Invalid arguments; one argument expected. Using default value of 60 seconds.\n");
        target_time = 60;
    }
    else target_time = strtoul(argv[1], NULL, 10);
    if (target_time == 0) {
        printf("Invalid argument, using default value of 60 seconds.\n");
        target_time = 60;
    }

    int n = 2; // Tracks how many primes have been found

    /*
    p will be checked for primeness and incremented by delta.
    Every prime >3 is of the form 6n +/- 1. So delta alternates between
    2 and 4 such that p is always of the above form
    */
    int p = 1;
    int delta = 4;
    int isPrime;

    double curr_time = 0;
    while (curr_time < target_time) {
        do {
            p += delta;
            delta = 2 * (delta % 3); // such that 2 -> 4 and 4 -> 2
            isPrime = 1;
            /*
            p is divided by all numbers up to its square root. If the
            remainder of this division is ever zero, p is not prime.
            */
            int i;
            for (i = 5; i < p; i++) {
                if (i * i > p) {
                    break;
                }
                if (p % i == 0) {
                    isPrime = 0;
                    break;
                }
            }
        } while (!isPrime);
        n++;
        curr_time = (double) (clock() - start_time) / CLOCKS_PER_SEC;
    }

    printf("A total of %d primes were found. The last prime found was %d\n", n, p);
    return 0;
}
