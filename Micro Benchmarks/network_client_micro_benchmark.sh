#!/bin/bash
timestamp=$(date +%s)
# run networking bandwith and latency benchmarks for 1 minute each
# variable $1 is the IP address of the machine running qperf in server mode
qperf -t 60 -v $1 tcp_bw tcp_lat > network_output_$timestamp.txt
