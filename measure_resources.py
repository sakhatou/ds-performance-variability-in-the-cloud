import sys
import time
import datetime
import psutil

# default time is 30 min
time_in_seconds = 1800

try:
	time_in_seconds = int(sys.argv[1])
except:
	print('Invalid arguments, using default values')

	
# set timestamp in filename
file_name = 'metrics_%s.csv' % int(time.time())

file = open(file_name, 'w')
header = 'time, cpu_percent, memory_percent, memory_used, disc_read_bytes, disc_write_bytes, disc_read_time, disc_write_time, net_bytes_sent, net_bytes_recv\n'
file.write(header)

# loop for x seconds
starttime = time.time()
timer = 0
while timer < time_in_seconds:
	clock = datetime.datetime.now()
	cpu = psutil.cpu_percent(interval=0.9)
	memory = psutil.virtual_memory()
	disk_io = psutil.disk_io_counters()
	net_io =  psutil.net_io_counters()

	record = '%s, %s, %s, %s, %s, %s, %s, %s, %s, %s\n' % (clock, cpu, memory.percent, memory.used, disk_io.read_bytes, disk_io.write_bytes, disk_io.read_time, disk_io.write_time, net_io.bytes_sent, net_io.bytes_recv)
	file.write(record)
	
	# update time elapsed
	timer = time.time() - starttime

file.close()
