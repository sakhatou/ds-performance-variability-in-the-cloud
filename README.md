# Distributed Systems
## Performance Variability in the Cloud  - Group 15
### Instructions

Start VM (2vCpu, 8GB Mem)
You can run the Python script for manageing AWS services (after configuration of credentials)
You can compile scripts in Azure folder for deployment by pre-prepared template (after configuration of credentials)


SSH into VM

Clone repository
```
git clone https://sakhatou@bitbucket.org/sakhatou/ds-performance-variability-in-the-cloud.git
```

Install requirements
```
bash requirements.sh
pip install -r requirements.txt
```

Run benchmark scripts seperately
Spark job
```
bash overall_benchmark.sh
```
#### Micro benchmarks
```
cd Micro\ Benchmarks/
```

Cpu benchmark:
```
bash cpu_micro_benchmark.sh
```

Memory benchmark:
```
bash memory_micro_benchmark.sh
```

Disc benchmark:
```
bash disc_io_micro_benchmark.sh
```
Network benchmark:
Start second VM
Run command on second VM
```
qperf
```

Run command on first VM with private IP set in script
```
bash network_client_micro_benchmark.sh
```