#!/bin/bash
sudo apt -y update
sudo apt -y install gcc
sudo apt -y install make
sudo apt -y install python3
sudo apt -y install python3-pip
sudo apt -y install openjdk-8-jdk
sudo apt -y install bonnie++

export PYSPARK_PYTHON=/usr/bin/python3

sudo pip install -r requirements.txt
sudo pip3 install -r requirements.txt

wget https://www.openfabrics.org/downloads/qperf/qperf-0.4.9.tar.gz
tar xf qperf-0.4.9.tar.gz
cd qperf-0.4.9
./configure
make
make install
cd ..
