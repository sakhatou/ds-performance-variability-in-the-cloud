tcp_bw:
    bw              =   119 MB/sec
    msg_rate        =  1.82 K/sec
    time            =    60 sec
    send_cost       =  8.39 sec/GB
    recv_cost       =   260 ms/GB
    send_cpus_used  =   100 % cpus
    recv_cpus_used  =   3.1 % cpus
tcp_lat:
    latency        =  139 us
    msg_rate       =  7.2 K/sec
    time           =   60 sec
    loc_cpus_used  =  100 % cpus
    rem_cpus_used  =  1.4 % cpus
