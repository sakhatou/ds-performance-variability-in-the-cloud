tcp_bw:
    bw              =   120 MB/sec
    msg_rate        =  1.83 K/sec
    time            =    60 sec
    send_cost       =  8.35 sec/GB
    recv_cost       =   248 ms/GB
    send_cpus_used  =   100 % cpus
    recv_cpus_used  =  2.97 % cpus
tcp_lat:
    latency        =   146 us
    msg_rate       =  6.87 K/sec
    time           =    60 sec
    loc_cpus_used  =   100 % cpus
    rem_cpus_used  =  1.72 % cpus
