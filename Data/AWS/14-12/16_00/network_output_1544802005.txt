tcp_bw:
    bw              =   120 MB/sec
    msg_rate        =  1.82 K/sec
    time            =    60 sec
    send_cost       =  8.36 sec/GB
    recv_cost       =   227 ms/GB
    send_cpus_used  =   100 % cpus
    recv_cpus_used  =  2.72 % cpus
tcp_lat:
    latency        =   131 us
    msg_rate       =  7.62 K/sec
    time           =    60 sec
    loc_cpus_used  =   100 % cpus
    rem_cpus_used  =  1.48 % cpus
