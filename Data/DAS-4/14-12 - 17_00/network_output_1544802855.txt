tcp_bw:
    bw              =   447 MB/sec
    msg_rate        =  6.82 K/sec
    time            =    60 sec
    send_cost       =   677 ms/GB
    recv_cost       =  2.11 sec/GB
    send_cpus_used  =  30.3 % cpus
    recv_cpus_used  =  94.2 % cpus
tcp_lat:
    latency        =  17.3 us
    msg_rate       =    58 K/sec
    time           =    60 sec
    loc_cpus_used  =   5.9 % cpus
    rem_cpus_used  =  9.05 % cpus
