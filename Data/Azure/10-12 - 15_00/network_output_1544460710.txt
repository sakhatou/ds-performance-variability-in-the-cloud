tcp_bw:
    bw              =   113 MB/sec
    msg_rate        =  1.72 K/sec
    time            =    60 sec
    send_cost       =   315 ms/GB
    recv_cost       =   488 ms/GB
    send_cpus_used  =  3.55 % cpus
    recv_cpus_used  =   5.5 % cpus
tcp_lat:
    latency        =   279 us
    msg_rate       =  3.59 K/sec
    time           =    60 sec
    loc_cpus_used  =   3.2 % cpus
    rem_cpus_used  =  3.92 % cpus
