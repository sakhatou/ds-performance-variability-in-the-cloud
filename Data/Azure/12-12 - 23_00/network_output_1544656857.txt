tcp_bw:
    bw              =   113 MB/sec
    msg_rate        =  1.72 K/sec
    time            =    60 sec
    send_cost       =   149 ms/GB
    recv_cost       =   421 ms/GB
    send_cpus_used  =  1.68 % cpus
    recv_cpus_used  =  4.75 % cpus
tcp_lat:
    latency        =   303 us
    msg_rate       =   3.3 K/sec
    time           =    60 sec
    loc_cpus_used  =  3.18 % cpus
    rem_cpus_used  =  3.72 % cpus
