import sys
import time
from urllib.request import urlopen
from pyspark import SparkContext, SparkConf

def download_file(url, file_name, replicate_data):
	response = urlopen(url)
	data = response.read()
	file = open(file_name, 'wb')
	for loop in range(replicate_data):
		file.write(data)
	file.close()

def count_occurences(file):
	# create Spark context with Spark configuration
	conf = SparkConf().setAppName("Spark job - Count occurences")
	sc = SparkContext(conf=conf)
	
	# set threshold
	threshold = 2
	
	tokenized = sc.textFile(filename)
	
	# count the occurrence of each word
	wordCounts = tokenized.map(lambda word: (word, 1)).reduceByKey(lambda v1,v2:v1 +v2)
	
	# filter out words with fewer than threshold occurrences
	filtered = wordCounts.filter(lambda pair:pair[1] >= threshold)
	
	# count characters
	charCounts = filtered.flatMap(lambda pair:pair[0]).map(lambda c: c).map(lambda c: (c, 1)).reduceByKey(lambda v1,v2:v1 +v2)
	
	list = charCounts.collect()
	print(repr(list)[1:-1])
	sc.stop()
	
if __name__ == "__main__":
	# set start time 
	starttime = time.time()
	
	# default argument values
	time_in_seconds = 1800
	loop_data_download_file = 100
	
	# catch invalid arguments
	try:
		time_in_seconds = int(sys.argv[1])
		loop_data_download_file = int(sys.argv[2])
	except:
		print('Invalid arguments, using default values')
	
	# read text and store in file
	url = 'https://norvig.com/big.txt'
	filename = 'test.txt'
	
	download_file(url, filename, loop_data_download_file)
	
	timer = 0
	
	# run while time limit is not succeeded
	while timer < time_in_seconds:
		
		# execute spark job
		count_occurences(filename)
		
		# update time elapsed
		timer = time.time() - starttime
