#!/usr/bin/env python
import sys
import boto3
from botocore.exceptions import ClientError

session = boto3.session.Session(aws_access_key_id='aws_access_id',
                                aws_secret_access_key='aws_secret',
                                region_name='us-east-1')
 
ec2 = session.resource('ec2')

# create key pair for ec2
def create_key_pair(key_name):
	key_file_name = '%s.pem' % key_name
	outfile = open(key_file_name,'w')
	key_pair = ec2.create_key_pair(KeyName=key_name)
	KeyPairOut = str(key_pair.key_material)
	outfile.write(KeyPairOut)

# creates new ec2 instance and returns instance id
def create_instance(key_name):
	instance = ec2.create_instances(
		ImageId='ami-1e299d7e',
		MinCount=1,
		MaxCount=1,
		KeyName=key_name,
		InstanceType='t2.xlarge')
	return instance[0].id
		
# starts existing ec2 instance by id and returns the response		
def start_instance(instance_id):
	instance = ec2.Instance(instance_id)
	response = instance.start()
	return response
	
# stops ec2 instance by id and returns the response		
def stop_instance(instance_id):
	instance = ec2.Instance(instance_id)
	response = instance.stop()
	return response

# terminates ec2 instance by id and returns the response		
def terminate_instance(instance_id):
	instance = ec2.Instance(instance_id)
	response = instance.terminate()
	return response

# check for stopped instances, if found return id, else return False
def stopped_instances():
	instances = ec2.instances.filter(
		Filters=[{'Name': 'instance-state-name', 'Values': ['stopped']}])
	
	if len(instances) > 0:
		return instances[0].id
	else:	
		return False

# return instance public ip		
def get_instance_ip(instance_id):
	instance = ec2.Instance(instance_id)
	return instance.public_ip_address

# interface
def select_option():
	print("Select AWS action:")
	print("Create VM: Press 1")
	print("Start VM: Press 2")
	print("Get VM IP: Press 3")
	print("Terminate VM: Press 4")
	print("Exit: Press 5")

	prompt = 0

	try:
		prompt = int(input("Press preferred option: "))
	except ValueError:
		print("Non-numeric input")
		select_option()

	if prompt == 1:
		try:
			key = input("Enter key name: ")
			create_key_pair(key)
			id = create_instance(key)
			print("New instance id is %s" % id)
			select_option()
		except:
			select_option()
	elif prompt == 2:
		try:
			id = input("Enter instance id: ")
			response = start_instance(id)
			print(response)
			select_option()
		except:
			select_option()			
	elif prompt == 3:
		try:	
			id = input("Enter instance id: ")
			ip = get_instance_ip(id)
			print("Public IP: %s" % ip)
			select_option()
		except:
			select_option()				
	elif prompt == 4: 
		try:		
			id = input("Enter instance id: ")
			response = terminate_instance(id)
			print(response)
		except:
			select_option()				
	elif prompt == 5: 
		exit()
	else:
		print("Please select a valid number")
		select_option()

# call main interface
select_option()